/*
    SPDX-FileCopyrightText: 2015 Elvis Angelaccio <elvis.angelaccio@kde.org>

    SPDX-License-Identifier: BSD-2-Clause
*/

#include "behaviorsettingspage.h"

namespace Kerfuffle
{
BehaviorSettingsPage::BehaviorSettingsPage(QWidget *parent, const QString &name, const QString &iconName)
    : SettingsPage(parent, name, iconName)
{
    setupUi(this);
}
}

#include "moc_behaviorsettingspage.cpp"

/*
    SPDX-FileCopyrightText: 2015 Elvis Angelaccio <elvis.angelaccio@kde.org>

    SPDX-License-Identifier: BSD-2-Clause
*/

#ifndef BEHAVIORSETTINGSPAGE_H
#define BEHAVIORSETTINGSPAGE_H

#include "settingspage.h"
#include "ui_behaviorsettingspage.h"

namespace Kerfuffle
{
class KERFUFFLE_EXPORT BehaviorSettingsPage : public SettingsPage, public Ui::BehaviorSettingsPage
{
    Q_OBJECT

public:
    explicit BehaviorSettingsPage(QWidget *parent = nullptr, const QString &name = QString(), const QString &iconName = QString());
};
}

#endif
